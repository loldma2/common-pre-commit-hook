This is a simple git hook config for pre-commit, which runs a checkstyle check on the staged files. 
Usage:

- From the root directory of the project in terminal run:
```
mkdir .git/hooks
cd .git/hooks
ln -s -f ../../hooks/pre-commit ./pre-commit
```
- You're done